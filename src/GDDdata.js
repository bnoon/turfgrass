import React, { Component } from "react";
import { extendObservable, autorun, action, reaction, when } from "mobx";
import { observer } from "mobx-react";
import axios from "axios";

const params = {
  sid: "304174",
  sdate: "20150501",
  edate: "20150620",
  elems: [
    {
      name: "gdd",
      base: 50,
      interval: [ 0, 0, 7 ],
      duration: "std",
      season_start: "03-15",
      reduce: "sum"
    }
  ]
};

function transformStnData(rawResult) {
  if (rawResult.data.error) {
    throw rawResult.data.error;
  }
  return rawResult.data.data.map(x => +x[1]);
}

export class GDDstore {
  constructor(lon, lat) {
    extendObservable(this, {
      location: { longitude: lon, latitude: lat },
      data: [],
      updateData: action(function(d) {
        this.data = d;
      })
    });
    when(() => this.data.length > 0, () => {
      autorun(() => console.log("Updating GDDstore: " + this.data));
      reaction(
        () => {
          return Object.values(this.location);
        },
        loc => {
          this.getACISdata();
        }
      );
    });
    this.getACISdata();
  }

  getACISdata() {
    return axios
      .post("http://data.rcc-acis.org/StnData", params)
      .then(res => {
        const d = transformStnData(res);
        this.updateData(d);
        return;
      })
      .catch(err => {
        console.log(
          "Request Error: " + (err.response.data || err.response.statusText)
        );
        this.updateData([]);
      });
  }
}

const GDDdata = observer(
  class GDDdata extends Component {
    constructor(props) {
      super(props);
      if (!props.store) {
        this.store = new GDDstore();
      }
    }

    render() {
      const store = this.props.store ? this.props.store : this.store;
      const childrenWithProps = React.Children.map(
        this.props.children,
        child =>
          React.cloneElement(child, { data: store.data.toJS(), ...child.props })
      );

      return <div>{childrenWithProps}</div>;
    }
  }
);

export default GDDdata;

