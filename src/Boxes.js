import React from "react";

import "./Boxes.css";

const threshold = 300;

let Boxes = ({ data }) => {
  let lst = data.map((datum, i) => {
    if (datum > threshold) {
      return <div className="Box over" key={i}></div>;
    } else {
      return <div className="Box" key={i}></div>;
    }
  });
  return (
    <div className="Boxes">
      {lst}
    </div>
  );
};

export default Boxes;

