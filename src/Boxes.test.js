import React from "react";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";
import Boxes from "./Boxes";

const mockdata = [ 100, 200, 250, 350, 400 ];
describe(Boxes, () => {
  it("renders and matches our snapshot", () => {
    const component = renderer.create(<Boxes data={mockdata} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders the correct number of boxes", () => {
    const d = mockdata;
    const wrapper = shallow(<Boxes data={mockdata} />);
    expect(wrapper.children().length).toBe(d.length);
  });

  it("sets the proper classes", () => {
    const d = mockdata;
    const wrapper = shallow(<Boxes data={mockdata} />);
    expect(wrapper.childAt(0).hasClass("over")).toBe(false);
    expect(wrapper.childAt(2).hasClass("over")).toBe(false);
    expect(wrapper.childAt(3).hasClass("over")).toBe(true);
    expect(wrapper.childAt(4).hasClass("over")).toBe(true);
  });
});

