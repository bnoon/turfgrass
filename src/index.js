import React from "react";
import ReactDOM from "react-dom";
import Boxes from "./Boxes";
import GDDdata, { GDDstore } from "./GDDdata";
import "./index.css";

const store = new GDDstore(-78, 42.5);

ReactDOM.render(
  <GDDdata store={store}>
    <Boxes />
  </GDDdata>,
  document.getElementById("root")
);

