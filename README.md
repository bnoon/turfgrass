# Example ReactJS component that loads ACIS data

## Getting started

* Use `nvm` to manage nodeJS versions.

* Use `yarn` as a `nodeJS` package manager.  Supliments `npm`.

* Use `create-react-app` to setup a component development environment.

* Use `jest` as a testing framework (default for `cra`).

* Use `mobx` for state management.

